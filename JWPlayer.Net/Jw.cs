﻿using System;
using System.Collections.Generic;
using JWPlayer.Net.Query;
using JWPlayer.Net.Response;
using RestSharp;
using RestSharp.Extensions;

namespace JWPlayer.Net
{
    public class Jw : IJw
    {
        private const string ApiFormat = "json";
        public string ApiKit { get; }
        public string Host = "api.bitsontherun.com";

        public string ApiUrl { get; private set; }
        public string ApiKey { get; }
        public string ApiSecret { get; }
        public string ApiVersion { get; set; }

        private readonly RestClient httpClient;
        public Jw(string apiKey, string apiSecret)
        {
            ApiKey = apiKey;
            ApiSecret = apiSecret;
            ApiKit = "1.3";
            ApiVersion = "v1";
            var url = $"http://{Host}/{ApiVersion}/";
            ApiUrl = url;
            httpClient = new RestClient(url);
        }

        public VideoListResponse ListVideos(BasicVideoSearch searchQuery)
        {
            var request = new RestRequest("videos/list", Method.GET);
            GenerateBaseQuery(request);

            if(!string.IsNullOrEmpty(searchQuery.Search))
                request.AddParameter("search", searchQuery.Search,ParameterType.QueryString);
            var startDateUnixFormat = searchQuery.StartDate.ToUnixTimeSeconds();
            request.AddParameter("start_date", startDateUnixFormat, ParameterType.QueryString);

            SignRequest(request);
            var response = httpClient.Execute<VideoListResponse>(request);
            response.Data.Json = response.Content;
            return response.Data;
        }

        public VideoListResponse ListVideos()
        {
            var request = new RestRequest("videos/list", Method.GET);
            GenerateBaseQuery(request);
            SignRequest(request);
            var response = httpClient.Execute<VideoListResponse>(request);
            response.Data.Json = response.Content;
            return response.Data;
        }

        public VideoCreateResponse CreateVideo(Dictionary<string, string> parameters)
        {
            var request = new RestRequest("videos/create", Method.GET);
            GenerateBaseQuery(request);

            foreach (var param in parameters)
            {
                if (param.Key.ToLower().Contains("date"))
                {
                    request.AddParameter(param.Key, DateTimeOffset.Parse(param.Value).ToUnixTimeSeconds(), ParameterType.QueryString);
                    continue;
                }
                request.AddParameter(param.Key, param.Value, ParameterType.QueryString);
            }
            SignRequest(request);
            var response = httpClient.Execute<VideoCreateResponse>(request);
            response.Data.Json = response.Content;
            return response.Data;
        }

        public VideoCreateResponse UpdateVideo(Dictionary<string, string> parameters)
        {
            var request = new RestRequest("videos/update", Method.GET);
            GenerateBaseQuery(request);

            foreach (var param in parameters)
            {
                if (param.Key.ToLower().Contains("date"))
                {
                    request.AddParameter(param.Key, DateTimeOffset.Parse(param.Value).ToUnixTimeSeconds(), ParameterType.QueryString);
                    continue;
                }
                request.AddParameter(param.Key, param.Value, ParameterType.QueryString);
            }
            SignRequest(request);
            var response = httpClient.Execute<VideoCreateResponse>(request);
            response.Data.Json = response.Content;
            return response.Data;
        }

        public VideoShowResponse ShowVideo(string videoKey)
        {
            var request = new RestRequest("videos/show", Method.GET);
            GenerateBaseQuery(request);
            request.AddParameter("video_key", videoKey, ParameterType.QueryString);
            SignRequest(request);
            var response = httpClient.Execute<VideoShowResponse>(request);
            response.Data.Json = response.Content;
            return response.Data;
        }

        public VideoShowThumbnailsResponse ShowVideoThumbails(string videoKey)
        {
            var request = new RestRequest("videos/thumbnails/show", Method.GET);
            GenerateBaseQuery(request);
            request.AddParameter("video_key", videoKey, ParameterType.QueryString);
            SignRequest(request);
            var response = httpClient.Execute<VideoShowThumbnailsResponse>(request);
            response.Data.Json = response.Content;
            return response.Data;
        }

        public VideoUpdateThumbnailsResponse UpdateVideoThumbails(string videoKey, int? size,  string position, string thumbnailIndex)
        {
            if (position.HasValue() && thumbnailIndex.HasValue())
                throw new ArgumentException("Use Position or ThumbnailIndex - Not Both");

            var request = new RestRequest("videos/thumbnails/update", Method.GET);
            GenerateBaseQuery(request);

            request.AddParameter("video_key", videoKey, ParameterType.QueryString);
            if(position.HasValue())
                request.AddParameter("position", position, ParameterType.QueryString);
            if (thumbnailIndex.HasValue())
                request.AddParameter("thumbnail_index", thumbnailIndex, ParameterType.QueryString);
            if (size.HasValue)
                request.AddParameter("size", videoKey, ParameterType.QueryString);
            SignRequest(request);
            var response = httpClient.Execute<VideoUpdateThumbnailsResponse>(request);
            response.Data.Json = response.Content;
            return response.Data;
        }

        private void GenerateBaseQuery(RestRequest request)
        {
            var timestamp = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
            timestamp = Convert.ToInt32(timestamp);
            request.AddParameter("api_format", ApiFormat, ParameterType.QueryString);
            request.AddParameter("api_key", ApiKey, ParameterType.QueryString);
            request.AddParameter("api_kit", ApiKit, ParameterType.QueryString);
            request.AddParameter("api_nonce", new Random().Next(10000000, 99999999).ToString(), ParameterType.QueryString);
            request.AddParameter("api_timestamp", int.Parse(timestamp.ToString()), ParameterType.QueryString);
        }

        private void SignRequest(RestRequest request)
        {
            request.AddParameter("api_signature", Hash.SignQueryString(request.Parameters, ApiSecret), ParameterType.QueryString);
        }
    }
}