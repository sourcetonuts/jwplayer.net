namespace JWPlayer.Net.Response
{
    public class Video
    {
        public string Status { get; set; }
        public int Updated { get; set; }
        public object ExpiresDate { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        public int Views { get; set; }
        public string Tags { get; set; }
        public object Sourceformat { get; set; }
        public string Mediatype { get; set; }
        public object UploadSessionId { get; set; }
        public Custom Custom { get; set; }
        public string Duration { get; set; }
        public object Sourceurl { get; set; }
        public object Link { get; set; }
        public object Author { get; set; }
        public string Key { get; set; }
        public object Error { get; set; }
        public int Date { get; set; }
        public string Md5 { get; set; }
        public string Sourcetype { get; set; }
        public string Size { get; set; }
    }
}