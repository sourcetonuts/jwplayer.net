namespace JWPlayer.Net.Response
{
    public class Videos
    {
        public int Total { get; set; }
        public Video Video { get; set; }
    }
}