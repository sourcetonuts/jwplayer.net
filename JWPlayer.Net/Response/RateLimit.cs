namespace JWPlayer.Net.Response
{
    public class RateLimit
    {
        public int Reset { get; set; }
        public int Limit { get; set; }
        public int Remaining { get; set; }
    }
}