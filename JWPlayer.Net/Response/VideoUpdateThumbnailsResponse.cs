﻿namespace JWPlayer.Net.Response
{
    public class VideoUpdateThumbnailsResponse : BaseResponse
    {
        public Thumbnail Thumbnail { get; set; }
        public Media Media { get; set; }
        public Link Link { get; set; }
    }
}
