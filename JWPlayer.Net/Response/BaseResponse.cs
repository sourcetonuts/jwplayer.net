namespace JWPlayer.Net.Response
{
    public class BaseResponse
    {
        public string Json { get; set; }
        public string Status { get; set; }
        public RateLimit RateLimit { get; set; }
        }
}