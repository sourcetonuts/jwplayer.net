﻿using System.Collections.Generic;

namespace JWPlayer.Net.Response
{
    public class VideoListResponse : BaseResponse
    {
        public List<Video> Videos { get; set; }
        public int Limit { get; set; }
        public int Offset { get; set; }
        public int Total { get; set; }
    }
}