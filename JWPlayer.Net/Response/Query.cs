namespace JWPlayer.Net.Response
{
    public class Query
    {
        public string Token { get; set; }
        public string Key { get; set; }
    }
}