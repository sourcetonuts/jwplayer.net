﻿namespace JWPlayer.Net.Response
{
    public class VideoShowResponse : BaseResponse
    {
        public Video Video { get; set; }
    }
}