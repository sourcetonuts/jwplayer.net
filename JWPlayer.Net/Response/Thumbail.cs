﻿namespace JWPlayer.Net.Response
{
    public class Thumbnail
    {
        public string Status { get; set; }
        public string StripStatus { get; set; }
        public string Key { get; set; }
    }
}
