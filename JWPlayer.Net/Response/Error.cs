namespace JWPlayer.Net.Response
{
    public class Error
    {
        public string Message { get; set; }
    }
}