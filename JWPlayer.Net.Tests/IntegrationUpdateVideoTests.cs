﻿using System;
using System.Collections.Generic;
using System.Configuration;
using NUnit.Framework;
using RestSharp.Extensions;

namespace JWPlayer.Net.Tests
{
    [TestFixture]
    public class IntegrationUpdateVideoTests
    {
        public string ApiKey { get; set; }
        public string ApiSecret { get; set; }

        [SetUp]
        public void Setup()
        {
            ApiKey = ConfigurationManager.AppSettings["ApiKey"];
            ApiSecret = ConfigurationManager.AppSettings["ApiSecret"];
        }

        [Test]
        public void UpdateVideo()
        {
            //Arrange
            var jw = new Jw(ApiKey, ApiSecret);
            
            var parameters = new Dictionary<string, string>
            {
                {"video_key", ConfigurationManager.AppSettings["TestVideoJWPlayerKey"]},
                {"title", "Test Updated"},
                {"tags", "foo, bar, updated"},
            };
            //Act
            var result = jw.UpdateVideo(parameters);
            //Assert
            Assert.IsTrue(result.Status.ToLower() == "ok");
        }

        [Test]
        public void UpdateVideoUsesUnixDateFormat()
        {
            //Arrange
            var jw = new Jw(ApiKey, ApiSecret);          
            var video = ConfigurationManager.AppSettings["TestVideoJWPlayerKey"];

            var expected = DateTimeOffset.UtcNow;
            var parameters = new Dictionary<string, string>
            {
                {"video_key", video},
                {"date", DateTimeOffset.UtcNow.ToString() }

            };
            jw.UpdateVideo(parameters);
            //Act
            var updated = jw.ShowVideo(video);
            //Assert
            Assert.AreEqual(expected.ToString("g"), DateTimeOffset.FromUnixTimeSeconds(updated.Video.Date).ToString("g"));
        }


        [Test]
        public void UpdateVideoThumbnail()
        {
            //Arrange
            var jw = new Jw(ApiKey, ApiSecret);
            var video = ConfigurationManager.AppSettings["TestVideoJWPlayerKey"];
            //Act           
            var result = jw.UpdateVideoThumbails(video, null, null, null);
            //Assert
            Assert.IsTrue(result.Link.Address.HasValue());
        }
    }
}
