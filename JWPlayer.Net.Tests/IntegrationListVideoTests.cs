﻿using System;
using System.Configuration;
using JWPlayer.Net.Query;
using NUnit.Framework;

namespace JWPlayer.Net.Tests
{
    [TestFixture]
    public class IntegrationListVideoTests
    {
        public string ApiKey { get; set; }
        public string ApiSecret { get; set; }

        [SetUp]
        public void Setup()
        {
            ApiKey = ConfigurationManager.AppSettings["ApiKey"];
            ApiSecret = ConfigurationManager.AppSettings["ApiSecret"];
        }

        [Test]
        public void GetListOfVideos()
        {
            //Arrange
            var jw = new Jw(ApiKey, ApiSecret);
            //Act
            var basicVideoSearch = new BasicVideoSearch {Search = "Foo", StartDate = DateTime.UtcNow.AddDays(-100)};
            var result = jw.ListVideos(basicVideoSearch);
            //Assert
            Assert.IsTrue(result.Videos.Count > 0);
        }

        [Test]
        public void CanUseRawJsonResponse()
        {
            //Arrange
            var jw = new Jw(ApiKey, ApiSecret);
            //Act
            var basicVideoSearch = new BasicVideoSearch { StartDate = DateTime.UtcNow.AddDays(-100) };
            var result = jw.ListVideos(basicVideoSearch);
            //Assert
            Assert.IsTrue(result.Json.Contains("Foo"));
        }

        [Test]
        public void ListAllVideos()
        {
            //Arrange
            var jw = new Jw(ApiKey, ApiSecret);
            //Act
            var result = jw.ListVideos();
            //Assert
            Assert.IsTrue(result.Json.Contains("Foo"));
        }
    }
}
