﻿using System;
using System.Collections.Generic;
using System.Configuration;
using NUnit.Framework;

namespace JWPlayer.Net.Tests
{
    [TestFixture]
    public class IntegrationCreateVideoTests
    {
        public string ApiKey { get; set; }
        public string ApiSecret { get; set; }

        [SetUp]
        public void Setup()
        {
            ApiKey = ConfigurationManager.AppSettings["ApiKey"];
            ApiSecret = ConfigurationManager.AppSettings["ApiSecret"];
        }

        [Test]
        public void CreateVideo()
        {
            //Arrange
            var jw = new Jw(ApiKey, ApiSecret);
            //Act
            var parameters = new Dictionary<string, string>
            {
                {"sourceurl", "http://www.sample-videos.com/video/mp4/720/big_buck_bunny_720p_1mb.mp4"},
                {"sourceformat", "mp4"},
                {"sourcetype", "url"},
                {"title", "Test"},
                {"description", "Test Video"},
                {"tags", "foo, bar"},
                {"custom.LegacyId", Guid.NewGuid().ToString()}
            };
            var result = jw.CreateVideo(parameters);
            //Assert
            Assert.IsTrue(result.Video.Key.Length > 0);
        }
    }
}
