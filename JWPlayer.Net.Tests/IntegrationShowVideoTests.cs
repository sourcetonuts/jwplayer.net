﻿using System;
using System.Collections.Generic;
using System.Configuration;
using NUnit.Framework;

namespace JWPlayer.Net.Tests
{
    [TestFixture]
    public class IntegrationShowVideoTests
    {
        public string ApiKey { get; set; }
        public string ApiSecret { get; set; }

        [SetUp]
        public void Setup()
        {
            ApiKey = ConfigurationManager.AppSettings["ApiKey"];
            ApiSecret = ConfigurationManager.AppSettings["ApiSecret"];
        }

        [Test]
        public void ShowVideo()
        {
            //Arrange
            var jw = new Jw(ApiKey, ApiSecret);
            //Act
            var video = ConfigurationManager.AppSettings["TestVideoJWPlayerKey"];
            var result = jw.ShowVideo(video);
            //Assert
            Assert.AreEqual(result.Video.Key, video);
        }

        [Test]
        public void ShowVideoThumbnails()
        {
            //Arrange
            var jw = new Jw(ApiKey, ApiSecret);
            //Act
            var video = ConfigurationManager.AppSettings["TestVideoJWPlayerKey"];
            var result = jw.ShowVideoThumbails(video);
            //Assert
            Assert.AreEqual(result.Thumbnail.Key, video);
        }
    }
}
