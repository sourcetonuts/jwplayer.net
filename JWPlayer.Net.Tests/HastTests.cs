﻿using System.Collections.Generic;
using NUnit.Framework;
using RestSharp;

namespace JWPlayer.Net.Tests
{
    [TestFixture]
    public class HastTests
    {
        [TestCase("I have spaces", "Key=I%20have%20spaces")] //Python UrlEncode is %20 and not + sign.
        [TestCase("//", "Key=%2F%2F")] //Python UrlEncode is Uppercase
        public void CorrectlyUrlEncodeParams(string input, string expected)
        {
            var result = Hash.PrepareQueryStrings(new List<Parameter> {new Parameter {Name = "Key", Value = input}});
            Assert.AreEqual(expected, result);
        }
    }
}
